module github.com/mayankshah1607/k8s-leader-election

go 1.16

require (
	k8s.io/apimachinery v0.24.2
	k8s.io/client-go v0.24.2
	k8s.io/klog v1.0.0
	sigs.k8s.io/controller-runtime v0.12.3
)
