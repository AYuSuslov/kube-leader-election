# Leader election in Kubernetes

A sample application for leader election in Kubernetes, process of LE restarts every 2 minutes.   
First version includes only the process of leader election.   
Second version adds extra API requests of all Namespaces available.
